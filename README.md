# Lossless Image Encoder+Decoder

Just for fun. It offers roughly equivalent file size to PNG but that is probably just because zstd 22 is better than zlib.

The most notable feature is that any prefix of the file is a rough approximation of the full file.

## Concept

The concept is trivial. It simply sends an "approximation" for the whole image, then refines it. The method of doing this is simple. First it sends a "reference colour" for the image. In practice this is the colour of the top-left pixel. Then it divides that region into four squares that cover the image (part of the squares may be "off canvas" but further detail is not sent for any region completely off canvas). Each of those regions is then divided in four and sent. This repeats until all of the detail is sent. If you for example drop the last half of the image you effectively downscale the image to 1/2 resolution in each dimension.

## Possible improvements.

- Right now pixels are sent in atomic chunks. It probably makes sense to separate the colour planes and send those. Especially for transparency which is always sent. If it was sent in it's own stream it would almost always be a stream of zeros and would compress to about nothing. But as it is now a zero every 4th byte it doesn't compress quite as well. I'm not sure if other colours would compress well like this as I suspect that brightness changes are more common that colour changes and brightness changes will affects all colours in the same way (roughly).
