use std::convert::TryInto;

struct PixelIter {
	width: u32,
	height: u32,
	stride: u32,
	x: u32,
	y: u32,
}

impl PixelIter {
	fn new(width: u32, height: u32) -> Self {
		PixelIter {
			width,
			height,
			stride: width.max(height).next_power_of_two(),
			x: 0,
			y: 0,
		}
	}
}

impl Iterator for PixelIter {
	type Item = (u32, u32);

	fn next(&mut self) -> Option<(u32, u32)> {
		if self.stride == 0 { return None }

		let r = Some((self.x, self.y));

		self.x += self.stride;
		if self.y & self.stride == 0 {
			self.x += self.stride
		}
		if self.x >= self.width {
			if self.y & self.stride == 0 {
				self.x = 0;
			} else {
				self.x = self.stride;
			}
			self.y += self.stride;

			if self.y >= self.height {
				self.stride /= 2;
				self.x = self.stride;
				self.y = 0;
			}
		}

		r
	}
}

fn base_pixel(x: u32, y: u32) -> (u32, u32) {
	let all_bits = x | y;
	let mask = all_bits - 1;
	(x & mask, y & mask)
}

fn subtract_pixel(l: image::Rgba<u8>, r: image::Rgba<u8>) -> image::Rgba<u8> {
	[
		l[0].wrapping_sub(r[0]),
		l[1].wrapping_sub(r[1]),
		l[2].wrapping_sub(r[2]),
		l[3].wrapping_sub(r[3]),
	].into()
}

fn encode_raw(mut out: impl std::io::Write, img: &mut image::RgbaImage) {
	out.write_all(&(img.width() as u64).to_le_bytes()).unwrap();
	out.write_all(&(img.height() as u64).to_le_bytes()).unwrap();

	for (x, y) in PixelIter::new(img.width(), img.height()) {
		let mut p = *img.get_pixel(x, y);
		if (x, y) != (0, 0) {
			let (basex, basey) = base_pixel(x, y);
			p = subtract_pixel(p, *img.get_pixel(basex, basey));
		};

		out.write_all(&p.0).unwrap();
	}
}

fn encode(out: impl std::io::Write, img: &mut image::RgbaImage) {
	encode_raw(zstd::Encoder::new(out, 22).unwrap().auto_finish(), img)
}

fn decode_raw(mut data: impl std::io::Read) -> image::RgbaImage {
	let mut buf = [0; 8];
	data.read_exact(&mut buf).unwrap();
	let width = u64::from_le_bytes(buf).try_into().unwrap();
	data.read_exact(&mut buf).unwrap();
	let height = u64::from_le_bytes(buf).try_into().unwrap();

	let mut img = image::RgbaImage::new(width, height);

	for (x, y) in PixelIter::new(width, height) {
		let (basex, basey) = base_pixel(x, y);

		let base = *img.get_pixel(basex, basey);

		let mut buf = [0; 4];
		data.read_exact(&mut buf).unwrap();

		img.put_pixel(x, y, [
			base[0].wrapping_add(buf[0]),
			base[1].wrapping_add(buf[1]),
			base[2].wrapping_add(buf[2]),
			base[3].wrapping_add(buf[3]),
		].into());
	}

	assert_eq!(data.read(&mut [0]).unwrap(), 0, "extra data");


	img
}

fn decode(data: impl std::io::Read) -> image::RgbaImage {
	decode_raw(zstd::Decoder::new(data).unwrap())
}

fn main() {
	let img = image::open(std::env::args().nth(1).unwrap()).unwrap().to_rgba8();

	let mut out = Vec::with_capacity(img.width() as usize * img.height() as usize);
	encode(&mut out, &mut img.clone());
	eprintln!("Encoded size: {}, vs png: {}, vs jpg: {}",
		out.len(),
		out.len() as f64 / 12014870.0,
		out.len() as f64 / 9421256.0);
	let out = decode(std::io::Cursor::new(out));

	// out.save("/tmp/out.tmp.png").unwrap();
	// std::fs::rename("/tmp/out.tmp.png", "/tmp/out.png").unwrap();

	assert_eq!((out.width(), out.height()), (img.width(), img.height()));
	assert!(out == img, "Image differs");
}
